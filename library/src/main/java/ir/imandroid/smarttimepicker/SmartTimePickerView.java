package ir.imandroid.smarttimepicker;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;


/**
 * Created by Gold on 13/10/2016.
 */
public class SmartTimePickerView extends LinearLayout implements View.OnClickListener ,View.OnLongClickListener,View.OnTouchListener {
    private View rootView;
    private ImageView img_cloud_1, img_cloud_2, img_cloud_3;
    private EditText edt_hour, edt_minute;
    private TypedArray typedArray;
    private FrameLayout lyt_clock;
    private LinearLayout up1,up2,down1,down2;
    private boolean hour_autoIncrement = false;
    private boolean minute_autoIncrement = false;
    private boolean hour_autoDecrement = false;
    private boolean minute_autoDecrement = false;
    private int REPEAT_DELAY = 600;
    private  int Hour_Minimum = 0;
    private int maximumHour = 24;
    private  int Minute_Minimum = 0;
    private  int maximumMinute = 60;
    private  int hourTextSize = 50;
    private  int minuteTextSize = 50;
    private  int hourTextColor = Color.parseColor("#FFFFFF");
    private  int minuteTextColor =  Color.parseColor("#FFFFFF");
    private Handler repeatUpdateHandler = new Handler();

        String temp1="";
        String temp2="";

    public SmartTimePickerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        rootView=inflate(context, R.layout.number_picker_view,this);
         typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.SmartTimePickerView, 0, 0);
        init();
        setAttribute();


    }

    private void setAttribute() {
        maximumHour =typedArray.getInteger(R.styleable.SmartTimePickerView_maximumHour, maximumHour);
        Hour_Minimum =typedArray.getInteger(R.styleable.SmartTimePickerView_minimumHour, Hour_Minimum);
        maximumMinute =typedArray.getInteger(R.styleable.SmartTimePickerView_maximumMinute, maximumMinute);
        Minute_Minimum=typedArray.getInteger(R.styleable.SmartTimePickerView_minimumMinute,Minute_Minimum);
        hourTextSize =typedArray.getInteger(R.styleable.SmartTimePickerView_hourTextSize, hourTextSize);
        minuteTextSize =typedArray.getInteger(R.styleable.SmartTimePickerView_minuteTextSize, minuteTextSize);
        hourTextColor =typedArray.getInteger(R.styleable.SmartTimePickerView_hourTextColor, hourTextColor);
        minuteTextColor =typedArray.getInteger(R.styleable.SmartTimePickerView_minuteTextColor, minuteTextColor);

        edt_hour.setTextSize(TypedValue.COMPLEX_UNIT_SP, hourTextSize);
        edt_minute.setTextSize(TypedValue.COMPLEX_UNIT_SP, minuteTextSize);

        edt_hour.setTextColor(hourTextColor);
        edt_minute.setTextColor(minuteTextColor);
    }

    private void init() {
        up1= (LinearLayout) rootView.findViewById(R.id.hour_up);
        up2= (LinearLayout) rootView.findViewById(R.id.minute_up);
        down1= (LinearLayout) rootView.findViewById(R.id.hour_down);
        down2= (LinearLayout) rootView.findViewById(R.id.minute_down);
        edt_hour = (EditText) rootView.findViewById(R.id.edt1);
        edt_minute = (EditText) rootView.findViewById(R.id.edt2);
        lyt_clock= (FrameLayout) rootView.findViewById(R.id.lyt_clock);
        img_cloud_1 = (ImageView) rootView.findViewById(R.id.img_abr1);
        img_cloud_2 = (ImageView) rootView.findViewById(R.id.img_abr2);
        img_cloud_3 = (ImageView) rootView.findViewById(R.id.img_abr3);

        checkChangeBackground();

//        edt_hour.setFilters(new InputFilter[]{new InputFilterMinMax(Hour_Minimum+"",maximumHour+"")});
//        edt_minute.setFilters(new InputFilter[]{new InputFilterMinMax(Minute_Minimum+"",maximumMinute+"")});

        edt_hour.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                temp1=charSequence.toString();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (edt_hour.getText().length()>0&&Integer.parseInt(edt_hour.getText().toString())>= maximumHour){
                    edt_hour.setText(temp1);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (edt_hour.getText().length()>0&&Integer.parseInt(edt_hour.getText().toString())>= maximumHour){
                    edt_hour.setText("");
                }
                checkChangeBackground();

            }
        });
        edt_minute.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                temp2=charSequence.toString();

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (edt_minute.getText().length()>0&&Integer.parseInt(edt_minute.getText().toString())>= maximumMinute){
                    edt_minute.setText(temp2);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (edt_minute.getText().length()>0&&Integer.parseInt(edt_minute.getText().toString())>= maximumMinute){
                    edt_minute.setText("");
                }
                checkChangeBackground();

            }
        });

        up1.setOnClickListener(this);
        up2.setOnClickListener(this);
        down1.setOnClickListener(this);
        down2.setOnClickListener(this);

        up1.setOnLongClickListener(this);
        up2.setOnLongClickListener(this);
        down1.setOnLongClickListener(this);
        down2.setOnLongClickListener(this);

        up1.setOnTouchListener(this);
        up2.setOnTouchListener(this);
        down1.setOnTouchListener(this);
        down2.setOnTouchListener(this);



    }

    private void checkChangeBackground() {
        int a=Integer.parseInt(edt_hour.getText().toString());
        if (a<6 || a>=18){
            lyt_clock.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.am));
            img_cloud_1.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.cloud_b));
            img_cloud_2.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.cloud_b));
            img_cloud_3.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.cloud_b));
        }else {
            lyt_clock.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.pm));
            img_cloud_1.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.cloud));
            img_cloud_2.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.cloud));
            img_cloud_3.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.cloud));
        }
    }


    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.hour_up) {
            increment(edt_hour, Hour_Minimum, maximumHour);

        } else if (i == R.id.minute_up) {
            increment(edt_minute, Minute_Minimum, maximumMinute);

        } else if (i == R.id.hour_down) {
            decrement(edt_hour, Hour_Minimum, maximumHour);

        } else if (i == R.id.minute_down) {
            decrement(edt_minute, Minute_Minimum, maximumMinute);

        }
    }

    @Override
    public boolean onLongClick(View view) {
        int i = view.getId();
        if (i == R.id.hour_up) {
            hour_autoIncrement = true;
            repeatUpdateHandler.post(new RepetitiveUpdater());

        } else if (i == R.id.minute_up) {
            minute_autoIncrement = true;
            repeatUpdateHandler.post(new RepetitiveUpdater());

        } else if (i == R.id.hour_down) {
            hour_autoDecrement = true;
            repeatUpdateHandler.post(new RepetitiveUpdater());

        } else if (i == R.id.minute_down) {
            minute_autoDecrement = true;
            repeatUpdateHandler.post(new RepetitiveUpdater());

        }
        return false;
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        int i = view.getId();
        if (i == R.id.hour_up) {
            if (event.getAction() != MotionEvent.ACTION_MOVE && hour_autoIncrement) {
                hour_autoIncrement = false;
                REPEAT_DELAY = 600;
            }

        } else if (i == R.id.minute_up) {
            if (event.getAction() != MotionEvent.ACTION_MOVE && minute_autoIncrement) {
                minute_autoIncrement = false;
                REPEAT_DELAY = 600;
            }

        } else if (i == R.id.hour_down) {
            if (event.getAction() != MotionEvent.ACTION_MOVE && hour_autoDecrement) {
                hour_autoDecrement = false;
                REPEAT_DELAY = 600;
            }
//

        } else if (i == R.id.minute_down) {
            if (event.getAction() != MotionEvent.ACTION_MOVE && minute_autoDecrement) {
                minute_autoDecrement = false;
                REPEAT_DELAY = 600;
            }

        }
        return false;
    }



    class RepetitiveUpdater implements Runnable {

        @Override
        public void run() {
            REPEAT_DELAY-=50;
            if (REPEAT_DELAY<=50){REPEAT_DELAY=50;}
            if (hour_autoIncrement) {
                increment(edt_hour, Hour_Minimum, maximumHour);
                repeatUpdateHandler.postDelayed(new RepetitiveUpdater(), REPEAT_DELAY);
            }else if (hour_autoDecrement) {
                decrement(edt_hour, Hour_Minimum, maximumHour);
                repeatUpdateHandler.postDelayed(new RepetitiveUpdater(), REPEAT_DELAY);
            }
            else if (minute_autoIncrement){
                increment(edt_minute,Minute_Minimum, maximumMinute);
                repeatUpdateHandler.postDelayed(new RepetitiveUpdater(), REPEAT_DELAY);
            }
            else if (minute_autoDecrement){
                decrement(edt_minute,Minute_Minimum, maximumMinute);
                repeatUpdateHandler.postDelayed(new RepetitiveUpdater(), REPEAT_DELAY);

            }
        }

    }



    private void decrement(EditText edtx,int min,int max) {
        int a=Integer.parseInt(edtx.getText().toString());
        a--;
         if (a<=min){
            a=max-1;
        }else if (a>=max){
            a=min;
        }
        edtx.setText(String.format("%02d",a));
        if (edtx== edt_hour){
            checkChangeBackground();
        }

    }

    public void setTypeFace(Typeface typeFace ) {
        edt_minute.setTypeface(typeFace);
        edt_hour.setTypeface(typeFace);
    }

    public Typeface getTypeFace(Typeface typeFace ) {
        return edt_minute.getTypeface();
    }

    public String getTextHour() {
        return edt_hour.getText().toString();
    }

    public void setTextHour(String  edt1) {
        this.edt_hour.setText(edt1);
    }

    public String getTextMinute() {
        return edt_minute.getText().toString();
    }

    public void setTextMinute(String edt2) {
        this.edt_minute.setText(edt2);
    }

    private void increment(EditText edtx, int min, int max) {
        int a=Integer.parseInt(edtx.getText().toString());
        a++;
            if (a<=min){
            a=max-1;
        }else if (a>=max){
            a=min;
        }
        edtx.setText(String.format("%02d",a));
        if (edtx== edt_hour){
        checkChangeBackground();

    }

    }


    public class InputFilterMinMax implements InputFilter {

        private int min, max;

        public InputFilterMinMax(int min, int max) {
            this.min = min;
            this.max = max;
        }

        public InputFilterMinMax(String min, String max) {
            this.min = Integer.parseInt(min);
            this.max = Integer.parseInt(max);
        }

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            try {
                int input = Integer.parseInt(dest.toString() + source.toString());
                if (isInRange(min, max, input))
                    return null;
            } catch (NumberFormatException nfe) { }
            return "";
        }

        private boolean isInRange(int min, int max, int input) {
            return max > min ? input >= min && input <= max : input >= max && input <= min;
        }
    }
}
