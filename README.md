# SmartTimePicker

![Alt Text](https://github.com/imansdn/SmartTimePicker/blob/master/smartTimePicker.gif)

### Add Maven to your root build.gradle

First of all Add it in your root build.gradle at the end of repositories:

```
allprojects {
  repositories {
    ...
    maven { url 'https://jitpack.io' }
  }
}
```
### Add Dependency

Add the dependency to your app build.gradle file

```
dependencies
{
   implementation 'com.github.imansdn:SmartTimePicker:1.0.4
}
```

And then sync your gradle and take a coffee.
